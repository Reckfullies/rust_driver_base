#![no_std]
#![feature(core_intrinsics)]

use core::intrinsics::abort;
pub use winapi::km::wdm::DbgPrint;

#[macro_export]
macro_rules! log {
    ($string: expr) => {
        unsafe {
            $crate::DbgPrint(concat!("[*] ", $string, "\0").as_ptr())
        }
    };

    ($string: expr, $($x:tt)*) => {
        unsafe {
            $crate::DbgPrint(concat!("[*] ", $string, "\0").as_ptr(), $($x)*)
        }
    };
}

#[export_name = "_fltused"]
static _FLTUSED: i32 = 0;

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    log!("PANIC OCCURRED!\n");
    #[allow(unused_unsafe)]
    unsafe {
        abort()
    }
}

#[no_mangle]
pub extern "system" fn driver_entry() -> u32 {
    log!("driver_entry\n");
    0
}
